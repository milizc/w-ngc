import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EsRequeridoDirective } from './directives/es-requerido.directive';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    EsRequeridoDirective,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    EsRequeridoDirective,
  ]
})
export class WNgcModule {
}
