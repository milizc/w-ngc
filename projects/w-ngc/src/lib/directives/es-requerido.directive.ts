import {
  Directive,
  ElementRef,
  Input,
  OnDestroy,
  Renderer2
} from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Directive({selector: '[wngEsRequerido]'})
export class EsRequeridoDirective implements OnDestroy {
  private static DesapareceCuandoEsValido: boolean = true;
  private campoAnalizado: AbstractControl;
  private span: any;
  private asterisco: any;

  constructor(private renderer: Renderer2,
              private el: ElementRef) {
    this.span = this.renderer.createElement('span');
    this.asterisco = this.renderer.createText(' (*)');
  }

  @Input() set wngEsRequerido(campo: AbstractControl) {
    this.campoAnalizado = campo;
    if (this.esCampoRequerido()) {
      this.agregarRequerido();
    }
    if (EsRequeridoDirective.DesapareceCuandoEsValido) {
      this.subscribirControl();
    }
  }

  public ngOnDestroy(): void {
    this.span = undefined;
    this.asterisco = undefined;
  }

  private esCampoRequerido() {
    if (!this.campoAnalizado) {
      return;
    }
    const validadores = this.campoAnalizado.validator &&
      this.campoAnalizado.validator(this.campoAnalizado);
    return validadores && validadores['required'];
  }

  private agregarRequerido() {
    this.renderer.appendChild(this.span, this.asterisco);
    this.renderer.appendChild(this.el.nativeElement, this.span);
  }

  private quitarRequerido() {
    if (this.renderer.parentNode(this.span) !== null) {
      this.renderer.removeChild(this.el.nativeElement, this.span);
    }
  }

  private subscribirControl() {
    this.campoAnalizado.statusChanges
      .subscribe((status) => {
        if (this.esCampoRequerido()) {
          this.agregarRequerido();
        } else {
          this.quitarRequerido();
        }
      });
  }
}
