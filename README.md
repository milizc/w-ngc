# WNgc

Librería que contiene directivas y componentes reutilizables para proyectos desarrollados en Angular > 4.0.0.

## Instalación

Los proyectos que consuman w-ngc debera agregar la siguientes línea en el archivo package.json

>   "dependencies": {
>           ...
>     "w-ngc": "^0.0.7"
>     }

## Directivas actuales

* wngEsRequerido: Agrega el **(*)** a labels en base a si su formControl asociado posee un validador Validators.required de @angular/forms.

## Cómo usarlo?

```[html]
<label for="txt_label_input"
      [wngEsRequerido]="form.get('formControlAsociado')">
   LABEL INPUT
</label>
<input id="txt_input" formControlName="formControlAsociado">
```

## Requerimientos

Angular >= 4.0.0, ir a https://www.npmjs.com/package/w-ngc
